package com.scentlab.chatbotbe.utils;

import com.google.cloud.dialogflow.v2.Context;
import com.google.cloud.dialogflow.v2.Intent;
import com.google.protobuf.ListValue;
import com.google.protobuf.Struct;
import com.google.protobuf.Value;
import com.scentlab.chatbotbe.constant.BespokeDialogFlowMessageCase;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class GoogleLibraryUtils {
    /**
     * Convert google value to java object, because response from google can contains different object types.
     *
     * @param value google value
     * @return java object
     */
    public static Object googleValueToJavaObject(Value value) {
        if (ObjectUtils.isEmpty(value)) {
            return null;
        }
//        Map<String, Object> result = new HashMap<>();
        switch (value.getKindCase()) {
            case STRING_VALUE:
                return value.getStringValue();
            case LIST_VALUE:
                List<Value> valueList = value.getListValue().getValuesList();
                List<Object> stringList = valueList.stream().map(ele -> googleValueToJavaObject(ele)).collect(Collectors.toList());
                return stringList;
            case STRUCT_VALUE:
                Map javaMap = googleStructToJavaMap(value.getStructValue());
                return javaMap;
            case BOOL_VALUE:
                return value.getBoolValue();
            case NUMBER_VALUE:
                return value.getNumberValue();
            case KIND_NOT_SET:
            case NULL_VALUE:
                return null;
        }
        return null;
    }

    public static Map googleStructToJavaMap(Struct struct) {
        if (ObjectUtils.isEmpty(struct)) {
            return null;
        }
        Map<String, Object> result = new HashMap<>();
        for (Map.Entry<String, Value> currField : struct.getFieldsMap().entrySet()) {
            result.put(currField.getKey(), googleValueToJavaObject(currField.getValue()));
        }
        return result;
    }

    public static Struct javaMapToGoogleStruct(Map<String, Object> map) {
        if (ObjectUtils.isEmpty(map)) {
            return Struct.newBuilder().build();
        }
        Struct struct = Struct.newBuilder().build();
        for (Map.Entry<String, Object> currField : map.entrySet()) {
            struct = struct.toBuilder().putFields(currField.getKey(), javaObjectToGoogleValue(currField.getValue())).build();
        }

        return struct;
    }

    public static Value javaObjectToGoogleValue(Object object) {
        if (ObjectUtils.isEmpty(object)) {
            return Value.newBuilder().build();
        }
        Value value = Value.newBuilder().build();
        if (object instanceof String) {
            value = value.toBuilder().setStringValue((String) object).build();
        } else if (object instanceof Collection) {
            Collection<Object> objectCollection = (Collection) object;
            ListValue listValue = ListValue.newBuilder().build();
            for (Object anObject : objectCollection) {
                Value anValue = javaObjectToGoogleValue(anObject);
                listValue = listValue.toBuilder().addValues(anValue).build();
            }
            value = value.toBuilder().setListValue(listValue).build();
        } else if (object instanceof Map) {
            Struct struct = javaMapToGoogleStruct((Map<String, Object>) object);
            value = value.toBuilder().setStructValue(struct).build();
        } else if ((object instanceof Integer) || (object instanceof Long) || (object instanceof Double)) {
            value = value.toBuilder().setNumberValue(Double.valueOf(object.toString())).build();
        }
        return value;
    }

    ////////////////////////////////
    public static Set<String> getParameterFromContext(Context context, String parameterName) {
        Map<String, Object> parameters = googleStructToJavaMap(context.getParameters());
        Object selectedPerfumeObj = parameters.get(parameterName);
        Set<String> selectedPerfume = null;
        if (selectedPerfumeObj instanceof List) {
            selectedPerfume = new HashSet<>();
            selectedPerfume.addAll((Collection<? extends String>) selectedPerfumeObj);
        } else if (selectedPerfumeObj instanceof String) {
            selectedPerfume = new HashSet<>();
            selectedPerfume.add((String) selectedPerfumeObj);
        }
        return selectedPerfume;
    }

    public static Map<BespokeDialogFlowMessageCase, Object> fulfillmentMessageListToResponse(List<Intent.Message> fulfillmentMessageList) {
        Map<BespokeDialogFlowMessageCase, Object> response = new HashMap<>();
        for (Intent.Message messages : fulfillmentMessageList) {
            switch (messages.getMessageCase()) {
                case PAYLOAD:
                    Struct payloadResult = messages.getPayload();
                    System.out.println(payloadResult);
                    response.put(BespokeDialogFlowMessageCase.PAYLOAD, googleStructToJavaMap(payloadResult));
                    break;
                case TEXT:
                    System.out.println(messages.getText());
                    List<String> texts = null;
                    for (int i = 0; i < messages.getText().getTextCount(); i++) {
                        if (texts == null) {
                            texts = new ArrayList<>();
                        }
                        texts.add(messages.getText().getText(i));
                    }
                    response.put(BespokeDialogFlowMessageCase.TEXT, texts);
                    break;
            }
        }
        return response;
    }
}

package com.scentlab.chatbotbe.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scentlab.chatbotbe.dto.bespokeservice.request.CreateBookingRequest;
import com.scentlab.chatbotbe.dto.bespokeservice.response.CreateBookingResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class RestTemplateUtils {
    public static CreateBookingResponse createBooking(String url, HttpEntity<CreateBookingRequest> request) throws JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
//        String url = "http://localhost:8082/bookings/save";

//        HttpEntity<CreateBookingRequest> requestObject = new HttpEntity<>(createBookingRequest);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, request, String.class);
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode responseJson = objectMapper.readTree(responseEntity.getBody());
        System.out.println(responseJson);
        return objectMapper.readValue(responseEntity.getBody(), CreateBookingResponse.class);
//        return null;
    }
}

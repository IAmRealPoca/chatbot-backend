package com.scentlab.chatbotbe.utils;

import com.google.api.client.json.webtoken.JsonWebToken;
import com.google.cloud.dialogflow.v2.Agent;
import com.google.cloud.dialogflow.v2.AgentsClient;
import com.google.cloud.dialogflow.v2.Context;
import com.google.cloud.dialogflow.v2.ContextName;
import com.google.cloud.dialogflow.v2.DetectIntentRequest;
import com.google.cloud.dialogflow.v2.DetectIntentResponse;
import com.google.cloud.dialogflow.v2.EntityType;
import com.google.cloud.dialogflow.v2.EntityTypesClient;
import com.google.cloud.dialogflow.v2.EventInput;
import com.google.cloud.dialogflow.v2.QueryInput;
import com.google.cloud.dialogflow.v2.QueryParameters;
import com.google.cloud.dialogflow.v2.QueryResult;
import com.google.cloud.dialogflow.v2.SessionName;
import com.google.cloud.dialogflow.v2.SessionsClient;
import com.google.cloud.dialogflow.v2.TextInput;
import com.google.protobuf.Struct;
import com.google.protobuf.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DialogflowService {
    public Map<String, QueryResult> detectIntentTexts(
            String projectId,
            List<String> texts,
            String sessionId,
            String languageCode
    ) throws IOException {
        Map<String, QueryResult> queryResults = new HashMap<>();
        //instantiates a client
        try (SessionsClient sessionsClient = SessionsClient.create()) {
            //Set the session name using the sessionId (UUID) and projectID
            SessionName session = SessionName.of(projectId, sessionId);
            System.out.println("Session Path: " + session.toString());
            //Detect intents for each text input
            for (String text : texts) {
                //Set the text (hello) and language code (en-US) for the query
                TextInput.Builder textInput =
                        TextInput.newBuilder().setText(text).setLanguageCode(languageCode);

                //Build the query with the TextInput
                QueryInput queryInput = QueryInput.newBuilder().setText(textInput).build();

                //Performs the detect intent request

                DetectIntentResponse response = sessionsClient.detectIntent(session, queryInput);

                //display the query result
                QueryResult queryResult = response.getQueryResult();

                queryResults.put(text, queryResult);

                System.out.println("Query text: " + queryResult.getQueryText());
                System.out.println("Detected intent: " + queryResult.getIntent().getDisplayName());
                System.out.println("Detected intent - confidence: " + queryResult.getIntentDetectionConfidence());
                System.out.println("Fulfillment text: " + queryResult.getFulfillmentText());
                System.out.println("Action: " + queryResult.getAction());
                List<Context> contexts = queryResult.getOutputContextsList();
                for (Context context : contexts) {
                    System.out.println("Lifespan: " + context.getLifespanCount() + " Context: " + context.getName());
                }

            }
        }
        return queryResults;
    }

    public Map<String, QueryResult> detectIntentEvent(
            String projectId,
            String events,
            String sessionId,
            String languageCode
    ) throws IOException {
        Map<String, QueryResult> queryResults = new HashMap<>();
        //instantiates a client
        try (SessionsClient sessionsClient = SessionsClient.create()) {
            //Set the session name using the sessionId (UUID) and projectID
            SessionName session = SessionName.of(projectId, sessionId);
            System.out.println("Session Path: " + session.toString());
            //Detect intents for each text input
                EventInput.Builder eventInput =
                        EventInput.newBuilder().setName(events).setLanguageCode(languageCode);

                QueryInput queryInput = QueryInput.newBuilder().setEvent(eventInput).build();

                DetectIntentResponse response = sessionsClient.detectIntent(session, queryInput);

                QueryResult queryResult = response.getQueryResult();

                queryResults.put(events, queryResult);
        }
        return queryResults;
    }

    public Map<String, QueryResult> detectIntentEventWithParam(
            String projectId,
            String events,
            Map<String, Object> parameters,
            String sessionId,
            String languageCode
    ) throws IOException {
        Map<String, QueryResult> queryResults = new HashMap<>();
        //instantiates a client
        try (SessionsClient sessionsClient = SessionsClient.create()) {
            //Set the session name using the sessionId (UUID) and projectID

            SessionName session = SessionName.of(projectId, sessionId);
            System.out.println("Session Path: " + session.toString());

            //Detect intents for each text input
//            for (String event : events) {
                Struct params = GoogleLibraryUtils.javaMapToGoogleStruct(parameters);

                EventInput.Builder eventInput =
                        EventInput.newBuilder().setName(events).setParameters(params).setLanguageCode(languageCode);

                QueryInput queryInput = QueryInput.newBuilder().setEvent(eventInput).build();


                DetectIntentRequest request =
                        DetectIntentRequest.newBuilder()
                                .setQueryInput(queryInput)
                                .setSession(session.toString()).build();

                DetectIntentResponse response = sessionsClient.detectIntent(request);

                QueryResult queryResult = response.getQueryResult();

                queryResults.put(events, queryResult);
//            }
        }
        return queryResults;
    }

    /**
     * Test create entity, succeed
     *
     * @param projectId
     * @throws IOException
     */
    public void createEntity(String projectId) throws IOException {
        EntityType.Entity entity = EntityType.Entity.newBuilder()
                .setValue("Bưởi")
                .addSynonyms("Năm roi")
                .addSynonyms("Sáu múi")
                .build();
        EntityType entityType = EntityType.newBuilder()
                .setDisplayName("created-by-java")
                .setKind(EntityType.Kind.KIND_MAP)
                .addEntities(entity).build();
//        entityType.

        EntityTypesClient entityTypesClient = EntityTypesClient.create();
        entityTypesClient.createEntityType("projects/" + projectId + "/agent", entityType);
        entityTypesClient.close();
    }

    /**
     * Test update entity, succeed
     *
     * @param projectId
     * @param languageCode
     * @param entityValueMap
     * @throws IOException
     */
    public void updateEntity(
            String projectId,
            String languageCode,
            Map<String, List<String>> entityValueMap,
            String entityId) throws IOException {

        EntityTypesClient entityTypesClient = EntityTypesClient.create();
        try {
            EntityType entityType =
                    entityTypesClient.getEntityType("projects/" + projectId + "/agent/entityTypes/" + entityId);
            if (!ObjectUtils.isEmpty(entityType)) {
                for (Map.Entry<String, List<String>> entityValue : entityValueMap.entrySet()) {
                    List<String> listEntityValue = entityValue.getValue();
                    EntityType.Entity entity = EntityType.Entity.newBuilder()
                            .setValue(entityValue.getKey())
                            .addAllSynonyms(listEntityValue)
                            .build();

                    entityType = entityType.toBuilder().addEntities(entity).build();
                }
                entityTypesClient.updateEntityType(entityType);
            }
        } finally {
            if (entityTypesClient != null) {
                entityTypesClient.close();
            }
        }
        //test update entity


        //end test update entity

        //everything uses client to crud
        //please don't delete the comment below, as I will come back to check them later
//        entityType.toBuilder().setDisplayName("updated-note-vacancy");
//        EntityType.Entity entity = entityType.getEntit
//        AgentsClient agent = AgentsClient.create();
//        agent.get
//        entityTypesClient.updateEntityType(entityType);
//        ProjectAgentName projectAgentName = ProjectAgentName.newBuilder().setProject("a").build();
    }

    public List<Map> listAllCurrentEntities(
            String projectId
    ) throws IOException {
        EntityTypesClient entityTypesClient = EntityTypesClient.create();
        try {
            Iterable<EntityType> entityTypeIterable =
                    entityTypesClient.listEntityTypes("projects/" + projectId + "/agent").iterateAll();
            List<Map> responses = null;
            for (EntityType e : entityTypeIterable) {
                System.out.println(e);
                if (responses == null) {
                    responses = new ArrayList<>();
                }
                Map<String, Object> response = new HashMap<>();
                response.put("displayName", e.getDisplayName());
                response.put("id", e.getName());
                responses.add(response);
            }
            return responses;
        } finally {
            if (entityTypesClient != null) {
                entityTypesClient.close();
            }
        }

//        return null;
    }

//    public Map<String, QueryResult> callIntentWithContextsAndParams(
//            String projectId,
//            String sessionId
//    ) throws IOException {
//        try (SessionsClient sessionsClient = SessionsClient.create()) {
//            SessionName session = SessionName.of(projectId, sessionId);
//            ContextName contextName =
//                    ContextName.newBuilder().setSession(session.toString())
//                            .setContext("perfume_create_for_self_individual").build();
//
//            Context context = Context.newBuilder()
//                    .setName(contextName.toString()).build();
//
//            QueryParameters queryParameters = QueryParameters.newBuilder()
//                    .addContexts(context)
//                    .build();
//        }
//    }
}

package com.scentlab.chatbotbe.utils;

import com.scentlab.chatbotbe.dto.bespokeservice.request.CreateBookingRequest;
import com.scentlab.chatbotbe.dto.bespokeservice.response.CreateBookingResponse;
import com.scentlab.chatbotbe.dto.bespokeservice.response.GetUserBookingResponse;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.List;

public class WebClientUtils {
    public static void createBooking(String url, CreateBookingRequest createBookingRequest) {
        WebClient webClient = getWebClient(url);

        Flux<CreateBookingResponse> flux = webClient.post()
                .accept(MediaType.ALL)
                .body(BodyInserters.fromValue(createBookingRequest))
                .retrieve()
                .bodyToFlux(CreateBookingResponse.class);
//        Subscriber
        flux.subscribe(e -> System.out.println(e));
        return;
    }

    public static List<GetUserBookingResponse> getBookingFromUsername(String url, String username) {
        WebClient webClient = getWebClient(url + "/" + username);

        ResponseEntity<List<GetUserBookingResponse>> flux = webClient.get()
                .accept(MediaType.ALL)
                .retrieve()
                .toEntityList(GetUserBookingResponse.class)
                .block();
//        flux.subscribe(e -> System.out.println(e));
        return flux.getBody();
    }

    private static WebClient getWebClient(String baseUrl) {
        WebClient webClient = WebClient.builder()
                .baseUrl(baseUrl)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
                .build();
        return webClient;
    }
}

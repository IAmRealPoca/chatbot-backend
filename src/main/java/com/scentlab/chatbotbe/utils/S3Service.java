package com.scentlab.chatbotbe.utils;

import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.WritableResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

@NoArgsConstructor
@Service
public class S3Service {

    private ResourceLoader resourceLoader;

    @Autowired
    public S3Service(@Qualifier("webApplicationContext") ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    public void downloadS3Object(String s3Url) throws IOException {
//        System.out.println("what?");
        Resource resource = resourceLoader.getResource(s3Url);
        File downloadedS3Object = new File(resource.getFilename());

        try (InputStream inputStream = resource.getInputStream()){
            File newFile = new File("./src/images/" + downloadedS3Object.getName());
            newFile.createNewFile();
//            Files.copy(inputStream, downloadedS3Object.toPath(), StandardCopyOption.REPLACE_EXISTING);
            Files.copy(inputStream, newFile.toPath(), StandardCopyOption.REPLACE_EXISTING);

        }
    }

    public String uploadToS3(MultipartFile multipartFile) throws IOException {
        String resourceName = String.format("%s_%s", System.currentTimeMillis(), multipartFile.getOriginalFilename());
        WritableResource writableResource =
                (WritableResource) resourceLoader.getResource(
                        String.format("%s/%s", "s3://patra/images", resourceName)
                );
        try (InputStream inputStream = multipartFile.getInputStream();
             OutputStream outputStream = writableResource.getOutputStream()) {
            byte[] buffer = new byte[1024];
            int bytesRead = 0;
            while ((bytesRead = inputStream.read(buffer)) > -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
        }
        return resourceName;
    }


}
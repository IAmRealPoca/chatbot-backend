package com.scentlab.chatbotbe.utils;

import com.scentlab.chatbotbe.dto.bespokeservice.request.CreateBookingRequest;
import com.scentlab.chatbotbe.dto.bespokeservice.response.CreateBookingResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;


public class AsyncRestTemplateUtils {

    private static final Logger logger = LoggerFactory.getLogger(AsyncRestTemplateUtils.class);

//    private final RestTemplate restTemplate;
//
//    public AsyncRestTemplateUtils(RestTemplate restTemplate) {
//        this.restTemplate = restTemplate;
//    }

    @Async
    public Future<CreateBookingResponse> createBooking(String url, CreateBookingRequest createBookingRequest) {
        logger.info("Create booking: start");
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        CreateBookingResponse response = restTemplate.postForObject(url, createBookingRequest, CreateBookingResponse.class);
        AsyncResult<CreateBookingResponse> asyncResult = new AsyncResult<>(response);
        logger.info("Create booking: end");
        return asyncResult;
    }
}

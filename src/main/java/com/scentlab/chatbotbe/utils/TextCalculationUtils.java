package com.scentlab.chatbotbe.utils;

import org.apache.commons.text.similarity.LongestCommonSubsequence;

public class TextCalculationUtils {

    public static double calculateLCS(CharSequence a, CharSequence b) {
        LongestCommonSubsequence longestCommonSubsequence = new LongestCommonSubsequence();
        double length = Double.valueOf(longestCommonSubsequence.apply(a, b));
        double percent = length / Math.min(a.length(), b.length()) * 100;
        return percent;
    }

    public static double calculateMatchPercent(CharSequence a, CharSequence b) {
        LongestCommonSubsequence longestCommonSubsequence = new LongestCommonSubsequence();
        double length = Double.valueOf(longestCommonSubsequence.apply(a, b));
        double percent = length / Math.max(a.length(), b.length()) * 100;
        return percent;
    }
}

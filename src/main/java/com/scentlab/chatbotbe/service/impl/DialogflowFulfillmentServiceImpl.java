package com.scentlab.chatbotbe.service.impl;

import com.google.cloud.dialogflow.v2.Context;
import com.google.cloud.dialogflow.v2.ContextName;
import com.google.cloud.dialogflow.v2.EventInput;
import com.google.cloud.dialogflow.v2.QueryResult;
import com.google.cloud.dialogflow.v2.WebhookRequest;
import com.google.cloud.dialogflow.v2.WebhookResponse;
import com.google.protobuf.Struct;
import com.scentlab.chatbotbe.constant.BespokeDialogFlowMessageCase;
import com.scentlab.chatbotbe.constant.ParameterConstants;
import com.scentlab.chatbotbe.entity.StoredUserDialogflow;
import com.scentlab.chatbotbe.entity.json.SessionContent;
import com.scentlab.chatbotbe.objmapper.SessionContentMapper;
import com.scentlab.chatbotbe.repository.StoredUserDialogflowRepository;
import com.scentlab.chatbotbe.service.DialogflowFulfillmentService;
import com.scentlab.chatbotbe.utils.GoogleLibraryUtils;
import com.scentlab.chatbotbe.utils.TextCalculationUtils;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DialogflowFulfillmentServiceImpl implements DialogflowFulfillmentService {


    private StoredUserDialogflowRepository storedUserDialogflowRepository;
    private SessionContentMapper sessionContentMapper;

    @Override
    public WebhookResponse handleFulfillment(WebhookRequest webhookRequest) {
        QueryResult queryResult = webhookRequest.getQueryResult();

        if ("perfume-select-perfume-reasktypo-no".equals(queryResult.getAction())) {
            WebhookResponse webhookResponse = WebhookResponse
                    .newBuilder()
                    .build();
            //update perfume_create_for_self_individual (cfsi) context to allow user enter wrong perfume name
            List<Context> cfsiContextList = queryResult.getOutputContextsList().stream()
                    .filter((e -> e.getName().contains("perfume_create_for_self_individual"))).collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(cfsiContextList) && cfsiContextList.size() > 0) {
                Context cfsiContext = cfsiContextList.get(0);
                int cfsiContextIndex = webhookResponse.getOutputContextsList().indexOf(cfsiContext);
                cfsiContext = cfsiContext.toBuilder().setLifespanCount(3).build();
                if (cfsiContextIndex >= 0) {
                    webhookResponse = webhookResponse.toBuilder().setOutputContexts(cfsiContextIndex, cfsiContext).build();
                } else {
                    webhookResponse = webhookResponse.toBuilder().addOutputContexts(cfsiContext).build();
                }
            }
            return webhookResponse;
        }

        if ("perfume-select-perfume".equals(queryResult.getAction()) || "perfume-select-perfume-reasktypo-yes".equals(queryResult.getAction())) {

            Map<String, Object> handledResult = handlePerfumeSelectPerfumeWebhook(queryResult, webhookRequest.getSession());

            List<Context> perfumeSelectedContext = queryResult.getOutputContextsList().stream()
                    .filter(e -> e.getName().contains("perfume_perfume_selected")).collect(Collectors.toList());
            Context context = null;
            //save list of selected perfumes in n context to display to user
            if (!CollectionUtils.isEmpty(perfumeSelectedContext) && perfumeSelectedContext.size() > 0) {
                context = perfumeSelectedContext.get(0);
                StoredUserDialogflow storedUserDialogflow = storedUserDialogflowRepository.findById(webhookRequest.getSession()).isPresent() ?
                        storedUserDialogflowRepository.getOne(webhookRequest.getSession()) : null;
                if (storedUserDialogflow != null) {
                    SessionContent sessionContent = sessionContentMapper.fromMapToSessionContent(storedUserDialogflow.getSessionContent());
                    Set<String> perfumeVacancy = new HashSet<>();
                    if (sessionContent == null) {
                        sessionContent = new SessionContent();
                        perfumeVacancy = sessionContent.getSelectedPerfumes();
                    }
                    Struct struct = Struct.newBuilder().putFields("perfume-vacancy", GoogleLibraryUtils.javaObjectToGoogleValue(perfumeVacancy)).build();

                    context = context.toBuilder().setParameters(struct).build();
                }
            }

            Struct customPayload = GoogleLibraryUtils.javaMapToGoogleStruct((Map<String, Object>) handledResult.get(ParameterConstants.CUSTOMPAYLOAD));

            WebhookResponse webhookResponse = WebhookResponse
                    .newBuilder()
                    .addOutputContexts(context)
                    .setPayload(customPayload)
                    .build();

            //trigger the reask count intent
            int cfsiContextLifeSpan = 1;
            int pspfContextLifespan = 2;
            Object perfumePickErrorType = handledResult.get(ParameterConstants.CUSTOMPAYLOAD_WEBHOOK_ERROR_TYPE);
            if (!ObjectUtils.isEmpty(perfumePickErrorType)) {
                switch ((String) handledResult.get(ParameterConstants.CUSTOMPAYLOAD_WEBHOOK_ERROR_TYPE)) {
                    case "perfume-reask-when-not-enough":
                    case "perfume-already-picked":
                        if (!"perfume-select-perfume.perfume-select-perfume-reask".equals(queryResult.getAction())) {
                            EventInput eventInput = EventInput.newBuilder().setName("PERFUME_SELECTED_REASK")
                                    .setParameters(customPayload).build();
                            webhookResponse = webhookResponse.toBuilder().setFollowupEventInput(eventInput).build();
                            cfsiContextLifeSpan = 2;
                        }
                        break;
                    case "perfume-near-match":
                        EventInput eventInput = EventInput.newBuilder().setName("PERFUME_SELECTED_REASK_TYPO")
                                .setParameters(customPayload).build();
                        webhookResponse = webhookResponse.toBuilder().setFollowupEventInput(eventInput).build();
                        cfsiContextLifeSpan = 3;
                        pspfContextLifespan = 4;
                        break;
                }
            }
            //update perfume_create_for_self_individual (cfsi) context to allow user enter wrong perfume name
            List<Context> cfsiContextList = queryResult.getOutputContextsList().stream()
                    .filter((e -> e.getName().contains("perfume_create_for_self_individual"))).collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(cfsiContextList) && cfsiContextList.size() > 0) {
                Context cfsiContext = cfsiContextList.get(0);
                int cfsiContextIndex = webhookResponse.getOutputContextsList().indexOf(cfsiContext);
                cfsiContext = cfsiContext.toBuilder().setLifespanCount(cfsiContextLifeSpan).build();
                if (cfsiContextIndex >= 0) {
                    webhookResponse = webhookResponse.toBuilder().setOutputContexts(cfsiContextIndex, cfsiContext).build();
                } else {
                    webhookResponse = webhookResponse.toBuilder().addOutputContexts(cfsiContext).build();
                }
            }

            //update reask followup context for confirmation from typo asking
//            if ("perfume-select-perfume-reasktypo".equals(queryResult.getAction())) {
            List<Context> pspfContextList = queryResult.getOutputContextsList().stream()
                    .filter((e -> e.getName().contains("perfume-select-perfume-followup"))).collect(Collectors.toList());
            if (!CollectionUtils.isEmpty(pspfContextList) && pspfContextList.size() > 0) {
                Context pspfContext = pspfContextList.get(0);
                int cfsiContextIndex = webhookResponse.getOutputContextsList().indexOf(pspfContext);
                pspfContext = pspfContext.toBuilder().setLifespanCount(pspfContextLifespan).build();
                if (cfsiContextIndex >= 0) {
                    webhookResponse = webhookResponse.toBuilder().setOutputContexts(cfsiContextIndex, pspfContext).build();
                } else {
                    webhookResponse = webhookResponse.toBuilder().addOutputContexts(pspfContext).build();
                }
            }
//            }
            return webhookResponse;
        }

        if ("perfume_detail_edit_perfume_add".equals(queryResult.getAction())) {
            WebhookResponse webhookResponse = WebhookResponse.newBuilder().build();
            Map<String, Object> response = checkInputAccuracyForReconfirm(queryResult);
            if (!CollectionUtils.isEmpty(response)) {
                webhookResponse = webhookResponse.toBuilder().setPayload(GoogleLibraryUtils.javaMapToGoogleStruct(response)).build();
            }
            return webhookResponse;
        }
        return null;
    }

    private Map<String, Object> checkInputAccuracyForReconfirm(QueryResult queryResult) {
        String userInputtedText = queryResult.getQueryText();
        List<String> perfumeEntityFromUserInputtedText = (List<String>) GoogleLibraryUtils.googleStructToJavaMap(queryResult.getParameters()).get("perfume-vacancy");
        Map<String, Object> response = new HashMap<>();

        userInputtedText = userInputtedText.toLowerCase();
        if (!CollectionUtils.isEmpty(perfumeEntityFromUserInputtedText) && perfumeEntityFromUserInputtedText.size() > 0)
            if (TextCalculationUtils.calculateMatchPercent(userInputtedText, perfumeEntityFromUserInputtedText.get(0)) < 100) {
                System.out.println("Text calculation match: " + TextCalculationUtils.calculateMatchPercent(userInputtedText, perfumeEntityFromUserInputtedText.get(0).toLowerCase()));
                Map<String, String> customPayload = new HashMap<>();
                customPayload.put(ParameterConstants.CUSTOMPAYLOAD_MESSAGE, "Bạn đã chọn " + userInputtedText + ", có phải ý của bạn là " + perfumeEntityFromUserInputtedText);

                response.put(ParameterConstants.CUSTOMPAYLOAD, customPayload);
                response.put(ParameterConstants.CUSTOMPAYLOAD_WEBHOOK_ERROR_TYPE, "perfume-near-match");
                return response;
            }
        return null;
    }

    private Map<String, Object> handlePerfumeSelectPerfumeWebhook(QueryResult queryResult, String session) {
        Map<String, Object> response =
                checkInputAccuracyForReconfirm(queryResult);
        if (!ObjectUtils.isEmpty(response)) {
            return response;
        }
        response = new HashMap<>();

        Map<String, Context> filteredContexts = new HashMap<>();
        List<Context> contexts = queryResult.getOutputContextsList();
        for (Context context : contexts) {
            ContextName contextName = ContextName.parse(context.getName());
            filteredContexts.put(contextName.getContext(), context);
        }
//         contexts.stream().filter(ele -> ele.getName().contains("perfume_perfume_selected")).collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(filteredContexts) && filteredContexts.size() > 0) {
            Context perfumePerfumeSelectedContext = filteredContexts.get("perfume_perfume_selected");
            Map<String, Object> parameters = GoogleLibraryUtils.googleStructToJavaMap(perfumePerfumeSelectedContext.getParameters());
            Set<String> selectedPerfume = new HashSet<>();
            selectedPerfume.addAll(GoogleLibraryUtils.getParameterFromContext(perfumePerfumeSelectedContext, "perfume-vacancy"));
            for (String perfume : selectedPerfume) {
                System.out.println("Perfume1: " + perfume);
            }

            //get user inputted perfumes in session
            Optional<StoredUserDialogflow> oldStoredData = storedUserDialogflowRepository.findById(session);
            if (oldStoredData.isPresent()) {
                SessionContent sessionContent = sessionContentMapper.fromMapToSessionContent(oldStoredData.get().getSessionContent());

                //get new perfume user has just inputted
                Context perfumeSelectPerfumeFollowup = filteredContexts.get("perfume-select-perfume-followup");
                Set<String> perfumeCustomerMoiNhapVao =
                        !CollectionUtils.isEmpty(
                                GoogleLibraryUtils.getParameterFromContext(perfumeSelectPerfumeFollowup, "perfume-vacancy")) ?
                                GoogleLibraryUtils.getParameterFromContext(perfumeSelectPerfumeFollowup, "perfume-vacancy") : new HashSet<>();

                if (!CollectionUtils.isEmpty(perfumeCustomerMoiNhapVao) && perfumeCustomerMoiNhapVao.size() > 0) {
                    selectedPerfume.addAll(perfumeCustomerMoiNhapVao);
                    for (String selectedPerfumeElemenet : perfumeCustomerMoiNhapVao) {
                        if (sessionContent.getSelectedPerfumes().stream().anyMatch(e -> e.equals(selectedPerfumeElemenet))) {
                            Map<String, Object> customPayload = new HashMap<>();
                            String mess = "Bạn đã chọn nước hoa này rồi. Xin bạn hãy chọn nước hoa khác.";
                            customPayload.put(ParameterConstants.CUSTOMPAYLOAD_MESSAGE, mess);
                            customPayload.put(ParameterConstants.CUSTOMPAYLOAD_CUSTOMDATA, new ArrayList<>());
                            response.put(ParameterConstants.CUSTOMPAYLOAD, customPayload);
                            response.put(ParameterConstants.CUSTOMPAYLOAD_WEBHOOK_ERROR_TYPE, "perfume-already-picked");
                            return response;
                        }
                    }
                }

                if (sessionContent.getSelectedPerfumes() != null) {
                    selectedPerfume.addAll(sessionContent.getSelectedPerfumes());
                }
            }
            for (String perfume : selectedPerfume) {
                System.out.println("Perfume2: " + perfume);
            }


            StoredUserDialogflow storedUserDialogflow = null;
            Optional<StoredUserDialogflow> optionalStoredUserDialogflow = storedUserDialogflowRepository.findById(session);
//            if (optionalStoredUserDialogflow.isPresent()) {
//                storedUserDialogflow = optionalStoredUserDialogflow.get();
//            } else {
                storedUserDialogflow = new StoredUserDialogflow();
                storedUserDialogflow.setSessionId(session);
//            }


            SessionContent sessionContent = new SessionContent();
            sessionContent.setAction(queryResult.getAction());
            sessionContent.setContexts(contexts.stream().map(ele -> ele.getName()).collect(Collectors.toList()));
            sessionContent.setParameters(parameters);
            sessionContent.setSelectedPerfumes(selectedPerfume);
            Map<String, Object> ssContent = new HashMap<>();
            ssContent.put("sessionContent", sessionContent);
            storedUserDialogflow.setSessionContent(ssContent);
            storedUserDialogflowRepository.save(storedUserDialogflow);

            if (selectedPerfume.size() <= 2) {
                String message = "Bạn đã chọn " + selectedPerfume.size() + " nước hoa. " +
                        "Chúng tôi cần thêm ít nhất " + (3 - selectedPerfume.size()) + " nước hoa nữa. Xin bạn hãy cho chúng tôi " +
                        "biết thêm thông tin.";

                //handle asking perfume
                if ("perfume-select-perfume".equals(queryResult.getAction()) || "perfume-select-perfume.perfume-select-perfume-reask".equals(queryResult.getAction())) {
                    List<Context> outputContext = queryResult.getOutputContextsList();
                    outputContext = outputContext.stream().filter(e -> e.getName().contains("perfume-select-perfume-followup")).collect(Collectors.toList());
                    response.put("outputContexts", outputContext);
                }

                Map<String, Object> customPayload = new HashMap<>();
                customPayload.put(ParameterConstants.CUSTOMPAYLOAD_MESSAGE, message);
                customPayload.put(ParameterConstants.CUSTOMPAYLOAD_CUSTOMDATA, new ArrayList<>());
                response.put(ParameterConstants.CUSTOMPAYLOAD, customPayload);
                response.put(ParameterConstants.CUSTOMPAYLOAD_WEBHOOK_ERROR_TYPE, "perfume-reask-when-not-enough");
            } else {
                Map<BespokeDialogFlowMessageCase, Object> messageListResponse
                        = GoogleLibraryUtils.fulfillmentMessageListToResponse(queryResult.getFulfillmentMessagesList());
                Map<String, Object> customPayload = (Map<String, Object>) messageListResponse.get(BespokeDialogFlowMessageCase.PAYLOAD);
                String message = "Bạn đã chọn nước hoa";
                if (customPayload == null) customPayload = new HashMap<>();
                customPayload.put(ParameterConstants.CUSTOMPAYLOAD_MESSAGE, message);
                response.put(ParameterConstants.CUSTOMPAYLOAD, customPayload);
//                    response.put(ParameterConstants.CUSTOMPAYLOAD_WEBHOOK_ERROR_TYPE, "normal");
            }
        }
        return response;
    }
}

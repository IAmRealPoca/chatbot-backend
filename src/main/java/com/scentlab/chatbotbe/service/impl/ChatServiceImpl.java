package com.scentlab.chatbotbe.service.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.cloud.dialogflow.v2.Context;
import com.google.cloud.dialogflow.v2.ContextName;
import com.google.cloud.dialogflow.v2.Intent;
import com.google.cloud.dialogflow.v2.QueryResult;
import com.google.cloud.dialogflow.v2.SessionName;
import com.google.protobuf.Struct;
import com.scentlab.chatbotbe.constant.BespokeDialogFlowMessageCase;
import com.scentlab.chatbotbe.constant.BespokeServiceUrls;
import com.scentlab.chatbotbe.constant.ParameterConstants;
import com.scentlab.chatbotbe.dto.bespokeservice.request.CreateBookingRequest;
import com.scentlab.chatbotbe.dto.bespokeservice.response.CreateBookingResponse;
import com.scentlab.chatbotbe.dto.bespokeservice.response.GetUserBookingResponse;
import com.scentlab.chatbotbe.dto.request.ChatRequest;
import com.scentlab.chatbotbe.dto.response.ChatResponse;
import com.scentlab.chatbotbe.entity.Account;
import com.scentlab.chatbotbe.entity.Booking;
import com.scentlab.chatbotbe.entity.ChatHistory;
import com.scentlab.chatbotbe.entity.Note;
import com.scentlab.chatbotbe.entity.NotePerfume;
import com.scentlab.chatbotbe.entity.PerfumeFragrantica;
import com.scentlab.chatbotbe.entity.StoredUserDialogflow;
import com.scentlab.chatbotbe.entity.UserDialogflowSession;
import com.scentlab.chatbotbe.entity.json.ChatDetail;
import com.scentlab.chatbotbe.entity.json.CustomerProfile;
import com.scentlab.chatbotbe.objmapper.SessionContentMapper;
import com.scentlab.chatbotbe.repository.AccountRepository;
import com.scentlab.chatbotbe.repository.BookingRepository;
import com.scentlab.chatbotbe.repository.NoteRepository;
import com.scentlab.chatbotbe.repository.PerfumeRepository;
import com.scentlab.chatbotbe.repository.StoredUserDialogflowRepository;
import com.scentlab.chatbotbe.repository.UserDialogflowSessionRepository;
import com.scentlab.chatbotbe.service.ChatService;
import com.scentlab.chatbotbe.utils.BespokeStringUtils;
import com.scentlab.chatbotbe.utils.DialogflowService;
import com.scentlab.chatbotbe.utils.GoogleLibraryUtils;
import com.scentlab.chatbotbe.utils.UUIDUtils;
import com.scentlab.chatbotbe.utils.WebClientUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

@Service
//@AllArgsConstructor
public class ChatServiceImpl implements ChatService {

    private final DialogflowService dialogflowService;
    private final NoteRepository noteRepository;
    private final AccountRepository accountRepository;
    private final PerfumeRepository perfumeRepository;
    private final BookingRepository bookingRepository;
    private final UserDialogflowSessionRepository userDialogflowSessionRepository;
    private final StoredUserDialogflowRepository storedUserDialogflowRepository;
    private final SessionContentMapper sessionContentMapper;

    //
    @Autowired
    public ChatServiceImpl(DialogflowService dialogflowService, NoteRepository noteRepository, AccountRepository accountRepository, PerfumeRepository perfumeRepository, BookingRepository bookingRepository, UserDialogflowSessionRepository userDialogflowSessionRepository, StoredUserDialogflowRepository storedUserDialogflowRepository, SessionContentMapper sessionContentMapper) {
        this.dialogflowService = dialogflowService;
        this.noteRepository = noteRepository;
        this.accountRepository = accountRepository;
        this.perfumeRepository = perfumeRepository;
        this.bookingRepository = bookingRepository;
        this.userDialogflowSessionRepository = userDialogflowSessionRepository;
        this.storedUserDialogflowRepository = storedUserDialogflowRepository;
        this.sessionContentMapper = sessionContentMapper;
    }

    @org.springframework.beans.factory.annotation.Value("${bespoke.dialogflow.projectid}")
    private String projectId;

    @org.springframework.beans.factory.annotation.Value("${bespoke.dialogflow.languagecode}")
    private String languageCode;


    @Override
    public ChatResponse chatWithDialogflow(ChatRequest chatRequest) {
        String sessionId = chatRequest.getSessionId();
        sessionId = processSessionId(sessionId);

        if (BespokeStringUtils.isBlankAndEmpty(chatRequest.getUserName())) {
            ChatResponse chatResponse = new ChatResponse();
            chatResponse.setSessionId(sessionId);
            chatResponse.setResponseType(BespokeDialogFlowMessageCase.TEXT.getNumber());
            chatResponse.setResponse("Xin bạn hãy nhập tên.");
            return chatResponse;
        }
        if ("sẵn sàng".equals(chatRequest.getChatContent())) {

        }
        try {
            List<String> userChats = chatRequest.getChatContent();
            Map<String, QueryResult> queryResults = dialogflowService.detectIntentTexts(projectId, userChats, sessionId, languageCode);
            ChatResponse chatResponse = doAfterChat(queryResults, userChats, sessionId, chatRequest.getUserName());
            return chatResponse;
        } catch (Exception e) {
            e.printStackTrace();
            ChatResponse chatResponse = new ChatResponse();
            chatResponse.setResponse("Something bad happened...");
            chatResponse.setSessionId(sessionId);
            chatResponse.setResponseType(BespokeDialogFlowMessageCase.ERROR.getNumber());
            return chatResponse;
        }
    }

    private void doBeforeChat(ChatRequest chatRequest) {

    }

    private List<GetUserBookingResponse> getCustomerBooking(String username) {
        List<GetUserBookingResponse> customerBookings = WebClientUtils.getBookingFromUsername(BespokeServiceUrls.BOOKING_SERVICE_FIND_BY_USERNAME_URL, username);
//        List<Booking> customerBookings = bookingRepository.findAllByAccount_Username(username);
        if (!CollectionUtils.isEmpty(customerBookings) && customerBookings.size() > 0) {
            return customerBookings;
        }
        return null;
    }

    private void saveChatToSession(String sessionId, Map<String, QueryResult> queryResultMap) {
//        SessionName sessionName = SessionName.of(projectId, sessionId);
        Optional<StoredUserDialogflow> optional = storedUserDialogflowRepository.findById(sessionId);
        StoredUserDialogflow storedUserDialogflow = null;
        if (optional.isPresent()) {
            storedUserDialogflow = optional.get();
        } else {
            storedUserDialogflow = new StoredUserDialogflow();

            storedUserDialogflow.setSessionId(sessionId);
        }

        Object chatLogObj = storedUserDialogflow.getChatLog();
        if (ObjectUtils.isEmpty(chatLogObj)) {
            chatLogObj = new ArrayList<>();
        }

        List<ChatDetail> chatLogs = (List<ChatDetail>) chatLogObj;
        if (CollectionUtils.isEmpty(chatLogs) || chatLogs.size() <= 0) {
            chatLogs = new ArrayList<>();
        }
        for (Map.Entry<String, QueryResult> entry : queryResultMap.entrySet()) {
            ChatDetail chatDetail = new ChatDetail();
            chatDetail.setChatUser("user");
            Map<BespokeDialogFlowMessageCase, Object> ggToSurrender = new HashMap<>();
            ggToSurrender.put(BespokeDialogFlowMessageCase.TEXT, Arrays.asList(entry.getKey()));
            chatDetail.setContent(ggToSurrender);
            LocalDateTime currentChatTime =LocalDateTime.now();
            chatDetail.setChatTime(currentChatTime);

            QueryResult queryResult = entry.getValue();
            Map<BespokeDialogFlowMessageCase, Object> fToRespect = GoogleLibraryUtils.fulfillmentMessageListToResponse(queryResult.getFulfillmentMessagesList());
            ChatDetail botChatDetail = new ChatDetail();
            botChatDetail.setChatUser("bot");
            botChatDetail.setContent(fToRespect);
            botChatDetail.setChatTime(currentChatTime);
            chatLogs.add(chatDetail);
            chatLogs.add(botChatDetail);
        }
        storedUserDialogflow.setChatLog(chatLogs);
        storedUserDialogflowRepository.save(storedUserDialogflow);


    }

    private ChatResponse doAfterChat(Map<String, QueryResult> queryResults, List<String> userChats, String sessionId, String username) throws JsonProcessingException, ExecutionException, InterruptedException {

//        saveChatToSession(sessionId, queryResults);

        Map<BespokeDialogFlowMessageCase, Object> handledResults = handleDialogFlowResponse(userChats, queryResults, username);
        if (!CollectionUtils.isEmpty(handledResults)) {
            ChatResponse chatResponse = new ChatResponse();
            chatResponse.setTime(String.valueOf(System.currentTimeMillis()));
            chatResponse.setSessionId(sessionId);
            for (Map.Entry<BespokeDialogFlowMessageCase, Object> handledResult : handledResults.entrySet()) {
                switch (handledResult.getKey()) {
                    case TEXT:
                    case CUSTOMIZED_PAYLOAD:
                    case PAYLOAD:
                        chatResponse.setResponse(handledResult.getValue());
                        chatResponse.setResponseType(handledResult.getKey().getNumber());
                        break;
                    case CUSTOMER_INFO_OBJECT:
                        saveInfoToBooking((Map<String, Object>) handledResult.getValue());
                        break;
                    case QUERYRESULT_ACTION:
                        chatResponse.setAction((String) handledResult.getValue());
                        break;
                    case QUERYRESULT_CONTEXTS:
                        List<Context> contexts = (List<Context>) handledResult.getValue();
                        break;
                }// end switch
            }// end for
            return chatResponse;
        }//end if collectionUtils.isEmpty(handledResult)
        return null;
    }

    private String processSessionId(String sessionId) {
        if (!BespokeStringUtils.isBlankAndEmpty(sessionId)) {
            if (userDialogflowSessionRepository.findById(sessionId).isPresent()) {
                UserDialogflowSession userDialogflowSession = userDialogflowSessionRepository.findById(sessionId).get();
                long elapsedTimeSeconds = Duration.between(userDialogflowSession.getSavedTime(), LocalDateTime.now()).getSeconds();
                elapsedTimeSeconds = elapsedTimeSeconds / 60;
                if (elapsedTimeSeconds > 15) {
                    userDialogflowSessionRepository.delete(userDialogflowSession);
                    sessionId = UUIDUtils.getUUID();
                    UserDialogflowSession newSession = new UserDialogflowSession();
                    newSession.setSessionId(sessionId);
                    newSession.setSavedTime(LocalDateTime.now());
                    userDialogflowSessionRepository.save(newSession);
                }
            } else {
                UserDialogflowSession newSession = new UserDialogflowSession();
                newSession.setSessionId(sessionId);
                newSession.setSavedTime(LocalDateTime.now());
                userDialogflowSessionRepository.save(newSession);
            }
        } else {
            sessionId = UUIDUtils.getUUID();
            UserDialogflowSession newSession = new UserDialogflowSession();
            newSession.setSessionId(sessionId);
            newSession.setSavedTime(LocalDateTime.now());
            userDialogflowSessionRepository.save(newSession);
        }
        return sessionId;
    }

    @Override
    public ChatResponse greetingDialogFlow(ChatRequest chatRequest) throws IOException, ExecutionException, InterruptedException {
        String sessionId = chatRequest.getSessionId();
        sessionId = processSessionId(sessionId);
        //delete this after be able to get username from login info
        if (BespokeStringUtils.isBlankAndEmpty(chatRequest.getUserName())) {
            ChatResponse chatResponse = new ChatResponse();
            chatResponse.setSessionId(sessionId);
            chatResponse.setResponseType(BespokeDialogFlowMessageCase.TEXT.getNumber());
            Map<String, String> response = new HashMap<>();
            response.put("message", "Xin bạn hãy nhập tên.");
            chatResponse.setResponse(response);
            return chatResponse;
        }

        List<GetUserBookingResponse> customerBookings = getCustomerBooking(chatRequest.getUserName());

        if (!CollectionUtils.isEmpty(customerBookings) && customerBookings.size() > 0) {
//            for (Booking booking : customerBookings) {
//                booking.getStatus()
//            }
            List<String> bookingStrings = customerBookings.stream()
                    .map(e -> e.toString()).collect(Collectors.toList());
            Map<String, Object> customPayload = new HashMap<>();
            customPayload.put("listBooking", bookingStrings);
            customPayload.put("noOfBookings", customerBookings.size());
            Map<String, QueryResult> result = dialogflowService.detectIntentEventWithParam(projectId, "GREETING_VIEW_BOOKING", customPayload, sessionId, languageCode);
            ChatResponse chatResponse = doAfterChat(result, Arrays.asList("GREETING_VIEW_BOOKING"), sessionId, chatRequest.getUserName());
            Map<String, Object> responsePayload = (Map<String, Object>) chatResponse.getResponse();
            responsePayload.put("customData", bookingStrings);
            chatResponse.setResponse(responsePayload);
            return chatResponse;
        } else {
            Map<String, QueryResult> result = dialogflowService.detectIntentEvent(projectId, "WELCOME_NEW_USER", sessionId, languageCode);
            ChatResponse chatResponse = doAfterChat(result, Arrays.asList("WELCOME_NEW_USER"), sessionId, chatRequest.getUserName());
            return chatResponse;
        }
    }

    @Override
    public ChatResponse testEvent(String event) throws IOException {
//        String sessionId = "abcdef";
//        Map<String, QueryResult> queryResults = dialogflowService.detectIntentEventWithParam(projectId, Arrays.asList(event), sessionId, languageCode);
//        System.out.println(queryResults.get(event).getFulfillmentText());
        return null;
    }

    private Map<BespokeDialogFlowMessageCase, Object> handleDialogFlowResponse(List<String> userText, Map<String, QueryResult> queryResults, String username) {
        for (Map.Entry<String, QueryResult> queryResult : queryResults.entrySet()) {
            //handle getting user information
            deleteSession(queryResult.getValue());
            if (queryResult.getKey().equals(userText.get(0))) {
                String action = queryResult.getValue().getAction();

//                if ("GreetingIntent-viewbooking-createnewperfume".equals(action)) {

//                    return (queryResult.getValue(), String username);
//                }
                //add action that we want to handle
                if ("perfume-select-detail".equals(action)) {
                    return handlePerfumeSelectDetail(queryResult.getValue());
                }// end if perfume-select-detail equals action

                else if ("perfume-inquiry".equals(action)) {
                    return handlePerfumeInquiry(queryResult.getValue());
                }//end if perfume-inquiry equals action


                //normal chat content case
                else {
                    Map<BespokeDialogFlowMessageCase, Object> response
                            = GoogleLibraryUtils.fulfillmentMessageListToResponse(queryResult.getValue().getFulfillmentMessagesList());
                    return response;
                }
            }//end if queryResult.getkey().equals(userText.get(0))
        }//end for queryResult : queryResults.entrySet
        return null;
    }

    private void deleteSession(QueryResult queryResult) {

        ContextName contextName = ContextName.parse(queryResult.getOutputContexts(0).getName());
//                storedUserDialogflowRepository.findById(contextName.getSession())
        if (queryResult.hasDiagnosticInfo()) {
            if (queryResult.getDiagnosticInfo().containsFields("end_conversation")) {
                Map<String, Object> resultDiagnosticInfo = GoogleLibraryUtils.googleStructToJavaMap(queryResult.getDiagnosticInfo());
                System.out.println(contextName.getSession());
                System.out.println(contextName.toString());
                if ((boolean) resultDiagnosticInfo.get("end_conversation")) {
                    SessionName sessionName = SessionName.of(contextName.getProject(), contextName.getSession());
                    Optional<StoredUserDialogflow> optionalStoredUserDialogflow = storedUserDialogflowRepository.findById(sessionName.toString());
                    if (optionalStoredUserDialogflow.isPresent()) {
                        System.out.println("Stored user session " + sessionName.toString() + " is exist! Will be deleted.");
                        storedUserDialogflowRepository.delete(optionalStoredUserDialogflow.get());
                    }

                    Optional<UserDialogflowSession> optionalUserDialogflowSession = userDialogflowSessionRepository.findById(contextName.getSession());
                    if (optionalStoredUserDialogflow.isPresent()) {
                        System.out.println("User session " + contextName.getSession() + " is exist, will be deleted.");
                        userDialogflowSessionRepository.delete(optionalUserDialogflowSession.get());
                    }
                }
            }
        }
    }

    private Map<BespokeDialogFlowMessageCase, Object> handlePerfumeSelectDetail(QueryResult queryResult) {
        List<Context> resultContexts = queryResult.getOutputContextsList();
        //get the context we want to handle
        //when parameters are not filled, the context perfume_selected_dialog_context will exists in the dialogflow response
        //when all parameters are filled, the context _dialog_context will not be send in response
        List<Context> filteredContext = resultContexts.stream()
                .filter(ele -> (ele.getName().contains("perfume_perfume_selected")))
                .collect(Collectors.toList());
        if (!CollectionUtils.isEmpty(filteredContext) && filteredContext.size() > 0) {
//            for (Context aContext : filteredContext) {
            Context aContext = filteredContext.get(0);
            if (aContext.hasParameters()) {
                Map<BespokeDialogFlowMessageCase, Object> response =
                        handleCustomerInformation(aContext, queryResult);

                return response;
            }// end if filteredContext.get(0).hasParameters()
//            }
        }//end !CollectionUtils.isEmpty(filteredContext) && filteredContext.size() > 0
        return null;
    }

    private Map<BespokeDialogFlowMessageCase, Object> handlePerfumeInquiry(QueryResult queryResult) {
        List<String> selectedNote = null;
        for (Context aContext : queryResult.getOutputContextsList()) {

            if (aContext.getParameters().containsFields(ParameterConstants.DIALOGFLOW_NOTE_VACANCY)) {
                selectedNote.addAll(GoogleLibraryUtils.getParameterFromContext(aContext, ParameterConstants.DIALOGFLOW_NOTE_VACANCY));
            }
        }
        List<Note> notes = new ArrayList<>();
        List<NotePerfume> notePerfumes = new ArrayList<>();
        for (String noteName : selectedNote) {
            List<Note> note = (List<Note>) noteRepository.findByName(noteName);
            if (!CollectionUtils.isEmpty(note) && note.size() > 0) {
                notes.add(note.get(0));
                notePerfumes.addAll(note.get(0).getNotePerfumes());
            }
        }

        Collection<PerfumeFragrantica> perfumeFragranticas = perfumeRepository.findAllByNotePerfumesIn(notePerfumes);

        //after processing
        List<Intent.Message> messagesList = queryResult.getFulfillmentMessagesList();
        for (Intent.Message messages : messagesList) {
            Struct payload = messages.getPayload();
            Map<String, Object> javaPayload = GoogleLibraryUtils.googleStructToJavaMap(payload);
            javaPayload.put(ParameterConstants.CUSTOMPAYLOAD_CUSTOMDATA, perfumeFragranticas.stream().map(e -> e.getName()).collect(Collectors.toList()));

            Map<BespokeDialogFlowMessageCase, Object> result = new HashMap<>();
            result.put(BespokeDialogFlowMessageCase.CUSTOMIZED_PAYLOAD, javaPayload);
            return result;
        }//end for messages : messagesList
        return null;
    }

    private Map<BespokeDialogFlowMessageCase, Object> handleCustomerInformation(Context aContext, QueryResult queryResult) {
        String username = (String) GoogleLibraryUtils.googleValueToJavaObject(aContext.getParameters().getFieldsMap().get("username"));
        String age = (String) GoogleLibraryUtils.googleValueToJavaObject(aContext.getParameters().getFieldsMap().get("age"));

        Set<String> selectedPerfume = new HashSet<>();
        Set<String> selectedNote = new HashSet<>();

        if (aContext.getParameters().containsFields(ParameterConstants.DIALOGFLOW_PERFUME_VACANCY)) {
            selectedPerfume.addAll(GoogleLibraryUtils.getParameterFromContext(aContext, ParameterConstants.DIALOGFLOW_PERFUME_VACANCY));
        }

        if (aContext.getParameters().containsFields(ParameterConstants.DIALOGFLOW_NOTE_VACANCY)) {
            selectedNote.addAll(GoogleLibraryUtils.getParameterFromContext(aContext, ParameterConstants.DIALOGFLOW_NOTE_VACANCY));
        }

        Map<BespokeDialogFlowMessageCase, Object> returnMap = null;
        if (queryResult.getAllRequiredParamsPresent()) {
            System.out.println("Username: " + username);
            System.out.println("Age: " + age);
            System.out.println("Selected note: " + selectedNote);
            System.out.println("Selected perfume: " + selectedPerfume);

            Map<String, Object> collectedUserInfo = new HashMap<>();
            collectedUserInfo.put("username", username);
            collectedUserInfo.put("age", age);
            collectedUserInfo.put("perfume", selectedPerfume);
            collectedUserInfo.put("note", selectedNote);

            Map<String, Object> customPayload = new HashMap<>();
            customPayload.put("message", queryResult.getFulfillmentText());
            customPayload.put("customData", collectedUserInfo);

            returnMap = new HashMap<>();
            returnMap.put(BespokeDialogFlowMessageCase.CUSTOMER_INFO_OBJECT, collectedUserInfo);
            returnMap.put(BespokeDialogFlowMessageCase.PAYLOAD, customPayload);
            returnMap.put(BespokeDialogFlowMessageCase.QUERYRESULT_CONTEXTS, queryResult.getOutputContextsList());
            returnMap.put(BespokeDialogFlowMessageCase.QUERYRESULT_ACTION, queryResult.getAction());

            //returnMap.put(BespokeDialogFlowMessageCase.CUSTOMER_INFO_OBJECT, userInfoEntity);
            return returnMap;
        } else {
            returnMap = new HashMap<>();
            returnMap.put(BespokeDialogFlowMessageCase.TEXT, queryResult.getFulfillmentText());
            return returnMap;
        }
    }

    private Future<CreateBookingResponse> saveInfoToBooking(Map<String, Object> userInfo) throws JsonProcessingException, ExecutionException, InterruptedException {
//        Account account = accountRepository.findById((String) userInfo.get("username")).get();
        Account account = accountRepository.findById("namns").get();
        Booking booking = new Booking();
        booking.setAccount(account);
        CustomerProfile customerProfile = new CustomerProfile("Nam", "hiphop", "unisex", Integer.parseInt((String) userInfo.get("age")));

        Collection<PerfumeFragrantica> perfumeFragranticas = perfumeRepository.findAllByNameIn((Collection<String>) userInfo.get("perfume"));

//        PerfumePicked perfumePicked = new PerfumePicked(perfumeFragranticas);
//        Map<String, Object> customAttributes = new HashMap<>();
//        customAttributes.put("customerProfile", customerProfile);
//        customAttributes.put("perfumes", perfumePicked);
//        booking.setCustomerAttributes(customAttributes);
//        bookingRepository.save(booking);

        CreateBookingRequest createBookingRequest = CreateBookingRequest.builder()
                .userId("namns")
                .type("make for self")
                .makeAppointmentDate("2020-08-20")
                .timeBookingId("1")
                .customerMessage("Sent from Chatbot Backend Not Async")
                .perfumeId(perfumeFragranticas.stream().map(e -> e.getPerfumeFragranticaId()).collect(Collectors.toList()))
                .customerProfile(customerProfile)
                .build();

        HttpEntity<CreateBookingRequest> request = new HttpEntity<>(createBookingRequest);
        WebClientUtils.createBooking(BespokeServiceUrls.BOOKING_SERVICE_SAVE_URL, createBookingRequest);
//        RestTemplateUtils.createBooking(BespokeServiceUrls.BOOKING_SERVICE_SAVE_URL, request);
//        AsyncRestTemplateUtils asyncRestTemplateUtils = new AsyncRestTemplateUtils();
//        Future<CreateBookingResponse> future = asyncRestTemplateUtils.createBooking(BespokeServiceUrls.BOOKING_SERVICE_SAVE_URL, createBookingRequest);
        return null;
    }
}

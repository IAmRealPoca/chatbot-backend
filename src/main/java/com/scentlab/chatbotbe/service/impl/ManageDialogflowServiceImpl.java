package com.scentlab.chatbotbe.service.impl;

import com.scentlab.chatbotbe.entity.Note;
import com.scentlab.chatbotbe.entity.PerfumeFragrantica;
import com.scentlab.chatbotbe.repository.NoteRepository;
import com.scentlab.chatbotbe.repository.PerfumeRepository;
import com.scentlab.chatbotbe.service.ManageDialogflowService;
import com.scentlab.chatbotbe.utils.DialogflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ManageDialogflowServiceImpl implements ManageDialogflowService {
    private final DialogflowService dialogflowService;
    private final NoteRepository noteRepository;
    private final PerfumeRepository perfumeRepository;

    @Autowired
    public ManageDialogflowServiceImpl(DialogflowService dialogflowService, NoteRepository noteRepository, PerfumeRepository perfumeRepository) {
        this.dialogflowService = dialogflowService;
        this.noteRepository = noteRepository;
        this.perfumeRepository = perfumeRepository;
    }

    @Override
    public List<Map> getAllCurrentEntities(String projectId) throws IOException {
        return dialogflowService.listAllCurrentEntities(projectId);
    }

    @Override
    public void updateNoteVacancyEntity(String entityId) throws IOException {
        List<Note> notes = noteRepository.findAll();
        Map<String, List<String>> entityValuesMap = new HashMap<>();
        for (Note note: notes) {
            entityValuesMap.put(note.getName(), Arrays.asList(note.getName()));
        }
        dialogflowService.updateEntity("newagent-1-eyyiay","en-US", entityValuesMap, entityId);
    }

    @Override
    public void updatePerfumeVacancyEntity(String entityId) throws IOException {
        List<PerfumeFragrantica> perfumes = perfumeRepository.findAll();
        Map<String, List<String>> entityValuesMap = new HashMap<>();
        for (PerfumeFragrantica perfume: perfumes) {
            entityValuesMap.put(perfume.getName(), Arrays.asList(perfume.getName()));
        }
        dialogflowService.updateEntity("newagent-1-eyyiay","en-US", entityValuesMap, entityId);
    }
}

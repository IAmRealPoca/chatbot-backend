package com.scentlab.chatbotbe.service;

import com.google.cloud.dialogflow.v2.WebhookRequest;
import com.google.cloud.dialogflow.v2.WebhookResponse;

public interface DialogflowFulfillmentService {
    WebhookResponse handleFulfillment(WebhookRequest webhookRequest);
}

package com.scentlab.chatbotbe.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ManageDialogflowService {
    List<Map> getAllCurrentEntities(String projectId) throws IOException;
    void updateNoteVacancyEntity(String entityId) throws IOException;
    void updatePerfumeVacancyEntity(String entityId) throws IOException;
}

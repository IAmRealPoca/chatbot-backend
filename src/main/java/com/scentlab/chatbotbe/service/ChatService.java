package com.scentlab.chatbotbe.service;

import com.google.cloud.dialogflow.v2.WebhookRequest;
import com.google.cloud.dialogflow.v2.WebhookResponse;
import com.scentlab.chatbotbe.dto.request.ChatRequest;
import com.scentlab.chatbotbe.dto.response.ChatResponse;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

public interface ChatService {
    ChatResponse chatWithDialogflow(ChatRequest chatRequest) throws IOException;

    ChatResponse greetingDialogFlow(ChatRequest chatRequest) throws IOException, ExecutionException, InterruptedException;

    ChatResponse testEvent(String event) throws IOException;
}

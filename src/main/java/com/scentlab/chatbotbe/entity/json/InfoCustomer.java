package com.scentlab.chatbotbe.entity.json;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InfoCustomer {
    private CustomerProfile customerProfile;
    private Collection<NoteAvg> noteAvgs;
    private Collection<PerfumePicked> perfumePickeds;
}

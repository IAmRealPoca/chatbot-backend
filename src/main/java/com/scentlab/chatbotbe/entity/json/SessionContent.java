package com.scentlab.chatbotbe.entity.json;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SessionContent {
    private List<String> contexts;
    private String action;
    private Map<String, Object> parameters;
    private Set<String> selectedPerfumes;
}

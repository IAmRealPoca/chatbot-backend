package com.scentlab.chatbotbe.entity.json;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CustomerProfile implements Serializable {
    private String gender;
    private String style;
    private String typePerfume;
    private Integer age;
}

package com.scentlab.chatbotbe.entity.json;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChatDetail {
    private String chatUser;
    private LocalDateTime chatTime;
//    private Integer index;
    private Object content;
}

package com.scentlab.chatbotbe.entity.json;

import com.scentlab.chatbotbe.entity.Note;
import com.scentlab.chatbotbe.entity.NotePerfume;
import com.scentlab.chatbotbe.entity.PerfumeFragrantica;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PerfumePicked {

    private Integer perfumeFragranticaId;
    private String name;
    private int vote;
    private int voters;
    private String image;
    private int longevityPoor;
    private int longevityWeak;
    private int longevityModerate;
    private int longevityLongLasting;
    private int longevityVeryLongLasting;
    private int sillageSoft;
    private int sillageModerate;
    private int sillageHeavy;
    private int sillageEnorMous;
    private String detailLink;
    private int gender;
    private List<Note> notes;

    public PerfumePicked(PerfumeFragrantica perfumeFragrantica) {
        this.perfumeFragranticaId = perfumeFragrantica.getPerfumeFragranticaId();
        this.name = perfumeFragrantica.getName();
        this.vote = perfumeFragrantica.getVote();
        this.voters = perfumeFragrantica.getVoters();
        this.longevityPoor = perfumeFragrantica.getLongevityPoor();
        this.longevityWeak = perfumeFragrantica.getLongevityWeak();
        this.longevityModerate = perfumeFragrantica.getLongevityModerate();
        this.longevityLongLasting = perfumeFragrantica.getLongevityLongLasting();
        this.longevityVeryLongLasting = perfumeFragrantica.getLongevityVeryLongLasting();
        this.sillageSoft = perfumeFragrantica.getSillageSoft();
        this.sillageModerate = perfumeFragrantica.getSillageModerate();
        this.detailLink = perfumeFragrantica.getDetailLink();
        this.gender = perfumeFragrantica.getGender();
        this.notes = perfumeFragrantica.getNotePerfumes().stream()
                .map(NotePerfume::getNote)
                .collect(Collectors.toList());
    }
}

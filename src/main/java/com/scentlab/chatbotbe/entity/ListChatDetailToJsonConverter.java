package com.scentlab.chatbotbe.entity;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scentlab.chatbotbe.entity.json.ChatDetail;
import com.scentlab.chatbotbe.utils.BespokeStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Converter(autoApply = true)
public class ListChatDetailToJsonConverter implements AttributeConverter<List<ChatDetail>, String> {

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public String convertToDatabaseColumn(List<ChatDetail> chatHistory) {

        if (ObjectUtils.isEmpty(chatHistory)) {
            return null;
        }
//                Map<String, Object> map = new HashMap<>();
//        map.put("sessionId", chatHistory.getSessionId() == null ? chatHistory.getSessionId() : "");
//        map.put("username", chatHistory.getUsername() == null ? chatHistory.getUsername() : "");
//        map.put("chatDetail", chatHistory.getChatDetails() == null ? chatHistory.getChatDetails() : "");

        String result = "{}";
        try {
            result = objectMapper.writeValueAsString(chatHistory);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public List<ChatDetail> convertToEntityAttribute(String stringObjectMap) {
        try {
            if (BespokeStringUtils.isBlankAndEmpty(stringObjectMap)) {
                return null;
            }

            List<ChatDetail> chatDetailList =
                    objectMapper.readValue(stringObjectMap, List.class);
            return chatDetailList;
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return null;
    }
}

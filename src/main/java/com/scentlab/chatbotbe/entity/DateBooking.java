package com.scentlab.chatbotbe.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "date_booking")
@Builder
public class DateBooking {
    @Id
    @Column(name = "date_booking_id")
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Integer dateBookingId;

    @Column(name = "date_created", length = 16)
    private LocalDate dateCreated;

    @JsonIgnore
    @OneToMany(mappedBy = "dateBooking", fetch = FetchType.LAZY)
    private Collection<DateTimeBooking> dateTimeBookings;
}

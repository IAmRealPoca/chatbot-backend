package com.scentlab.chatbotbe.entity;

import com.scentlab.chatbotbe.entity.json.ChatDetail;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "chat_history")
public class ChatHistory {
    @Id
    @Column(name = "chat_history_id")
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Long chatHistoryId;

    @Column(name ="session_id")
    private String sessionId;

    @Column(name = "username")
    private String username;

    @Column(name = "chat_details")
    @Convert(converter = ListChatDetailToJsonConverter.class)
    private List<ChatDetail> chatDetails;
}

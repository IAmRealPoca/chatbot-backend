package com.scentlab.chatbotbe.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "perfume_fragrantica")
public class PerfumeFragrantica {

    @Id
    @Column(name = "perfume_fragrantica_id")
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Integer perfumeFragranticaId;

    @Column(name = "name")
    private String name;

    @Column(name = "vote")
    private Integer vote;

    @Column(name = "voters")
    private Integer voters;

    @Column(name = "image")
    private String image;

    @Column(name = "longevity_poor")
    private Integer longevityPoor;

    @Column(name = "longevity_weak")
    private Integer longevityWeak;

    @Column(name = "longevity_moderate")
    private Integer longevityModerate;

    @Column(name = "longevity_longlasting")
    private Integer longevityLongLasting;

    @Column(name = "longevity_verylonglasting")
    private Integer longevityVeryLongLasting;

    @Column(name = "sillage_soft")
    private Integer sillageSoft;

    @Column(name = "sillage_moderate")
    private Integer sillageModerate;

    @Column(name = "sillage_heavy")
    private Integer sillageHeavy;

    @Column(name = "sillage_enormous")
    private Integer sillageEnorMous;

    @Column(name = "is_crawled")
    private Boolean isCrawled;

    @Column(name = "detail_link")
    private String detailLink;

    @Column(name = "gender")
    private Integer gender;

    @JsonIgnore
    @OneToMany(mappedBy = "perfumeFragrantica", fetch = FetchType.LAZY)
    private Collection<NotePerfume> notePerfumes;
}

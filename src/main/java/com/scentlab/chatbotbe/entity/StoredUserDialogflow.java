package com.scentlab.chatbotbe.entity;

import com.scentlab.chatbotbe.entity.json.ChatDetail;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.List;
import java.util.Map;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "stored_user_dialogflow_session")
public class StoredUserDialogflow {
    @Id
    @Column(name = "session_id")
    private String sessionId;

    @Column(name = "session_content")
    @Convert(converter = JsonToMapConverter.class)
    private Map<String, Object> sessionContent;

    @Column(name = "chat_details")
    @Convert(converter = ListChatDetailToJsonConverter.class)
    private List<ChatDetail> chatLog;
}

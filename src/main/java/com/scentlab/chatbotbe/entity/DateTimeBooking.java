package com.scentlab.chatbotbe.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "datetime_booking")
@Builder
public class DateTimeBooking {
    @Id
    @Column(name = "datetime_booking_id")
    @GenericGenerator(name = "generator", strategy = "increment")
    @GeneratedValue(generator = "generator")
    private Integer dateTimeBookingId;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "date_booking_id", referencedColumnName = "date_booking_id")
    private DateBooking dateBooking;

    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    @JoinColumn(name = "time_booking_id", referencedColumnName = "time_booking_id")
    private TimeBooking timeBooking;

    //Appointment
    //Busy
    @Column(name = "type", length = 16)
    private String type;

    @OneToMany(mappedBy = "dateTimeBooking")
    private Collection<Booking> bookings;

}

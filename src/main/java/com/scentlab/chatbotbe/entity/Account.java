package com.scentlab.chatbotbe.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "account")
public class Account {

    @Id
    @Column(name = "username", length = 16)
    private String username;

    @Column(name = "password", length = 16, nullable = false)
    private String password;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "photo")
    private String photo;

    @Column(name = "gender")
    private int gender;

    @Column(name = "address")
    private String address;

    @Column(name = "is_active")
    private int isActive;

    @JsonIgnore
    @OneToMany(mappedBy = "account")
    private Collection<Booking> bookings;

    @JsonIgnore
    @OneToMany(mappedBy = "account")
    private Collection<AccountRole> accountRoles;

}

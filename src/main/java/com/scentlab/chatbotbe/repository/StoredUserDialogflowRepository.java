package com.scentlab.chatbotbe.repository;

import com.scentlab.chatbotbe.entity.StoredUserDialogflow;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StoredUserDialogflowRepository extends JpaRepository<StoredUserDialogflow, String> {
}

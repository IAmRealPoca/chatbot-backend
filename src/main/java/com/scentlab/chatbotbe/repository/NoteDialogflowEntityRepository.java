package com.scentlab.chatbotbe.repository;

import com.scentlab.chatbotbe.entity.NoteDialogflowEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NoteDialogflowEntityRepository extends JpaRepository<NoteDialogflowEntity, Integer> {

}

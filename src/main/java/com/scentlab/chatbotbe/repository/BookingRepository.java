package com.scentlab.chatbotbe.repository;

import com.scentlab.chatbotbe.entity.Booking;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookingRepository extends JpaRepository<Booking, Integer> {
    List<Booking> findAllByAccount_Username(String accountUsername);
}

package com.scentlab.chatbotbe.repository;

import com.scentlab.chatbotbe.entity.NotePerfume;
import com.scentlab.chatbotbe.entity.PerfumeFragrantica;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface PerfumeRepository extends JpaRepository<PerfumeFragrantica, Integer> {

    Collection<PerfumeFragrantica> findAllByName(String perfumeName);

    Collection<PerfumeFragrantica> findAllByNameIn(Collection<String> perfumeNames);

    Collection<PerfumeFragrantica> findAllByNameLike(String perfumeName);

    Collection<PerfumeFragrantica> findAllByNotePerfumesIn(List<NotePerfume> notePerfumes);
}

package com.scentlab.chatbotbe.repository;

import com.scentlab.chatbotbe.entity.UserDialogflowSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDialogflowSessionRepository extends JpaRepository<UserDialogflowSession, String> {
}

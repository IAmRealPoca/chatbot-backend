package com.scentlab.chatbotbe.repository;

import com.scentlab.chatbotbe.entity.Note;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface NoteRepository extends JpaRepository<Note, Integer> {
    Collection<Note> findByName(String name);
}

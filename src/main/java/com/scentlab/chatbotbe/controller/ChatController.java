package com.scentlab.chatbotbe.controller;

import com.google.cloud.dialogflow.v2.WebhookRequest;
import com.google.cloud.dialogflow.v2.WebhookResponse;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import com.scentlab.chatbotbe.constant.ApiPath;
import com.scentlab.chatbotbe.dto.request.ChatRequest;
import com.scentlab.chatbotbe.dto.response.ChatResponse;
import com.scentlab.chatbotbe.service.ChatService;
import com.scentlab.chatbotbe.service.DialogflowFulfillmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping(ApiPath.CHAT_PATH)
public class ChatController {

    private final ChatService chatService;
    private final DialogflowFulfillmentService dialogflowFulfillmentService;

    @Autowired
    public ChatController(ChatService chatService, DialogflowFulfillmentService dialogflowFulfillmentService) {
        this.chatService = chatService;
        this.dialogflowFulfillmentService = dialogflowFulfillmentService;
    }

//    @GetMapping(ApiPath.SEND_CHAT)
//    public ResponseEntity<ChatResponse> chatResponse() {
//        return ResponseEntity.ok(new ChatResponse(1, "a", "12/11/2020", "123456789"));
//    }

    @PostMapping("/dialogflow")
    public ResponseEntity<ChatResponse> chatWithDialogFlow(
            @RequestBody ChatRequest chatRequest
    ) throws IOException {

        ChatResponse result = chatService.chatWithDialogflow(chatRequest);
//        System.out.println(result.);
        return ResponseEntity.ok(result);
    }

    @PostMapping("/dialogflow-greeting")
    public ResponseEntity<ChatResponse> greetingDialogFlow(
            @RequestBody ChatRequest chatRequest) throws IOException, ExecutionException, InterruptedException {
        ChatResponse result = chatService.greetingDialogFlow(chatRequest);
//        System.out.println(result.);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/user-session")
    public ResponseEntity<?> checkUserSession() {
        return ResponseEntity.ok(null);
    }

    @PostMapping("/test-check-event")
    public void testCheckEvent(String event) throws IOException {
        chatService.testEvent(event);
    }

    @GetMapping("/hello")
    public ResponseEntity<String> hello(String hello) {
        return ResponseEntity.ok(hello);
    }

    @PostMapping("/fulfillment")
    public ResponseEntity<?> fulfillment(
            @RequestBody String request) throws InvalidProtocolBufferException {
        long startTime = System.currentTimeMillis();
        System.out.println(request.toString());
        WebhookRequest.Builder webhookRequestBuilder = WebhookRequest.newBuilder();
        JsonFormat.parser().merge(request, webhookRequestBuilder);
        WebhookResponse webhookResponse = dialogflowFulfillmentService.handleFulfillment(webhookRequestBuilder.build());
        System.out.println(JsonFormat.printer().print(webhookResponse));
        long endTime = System.currentTimeMillis();
        long duration = endTime - startTime;
        System.out.println("Webhook execution time: " + duration);
        return ResponseEntity.ok(JsonFormat.printer().print(webhookResponse));
    }

}

package com.scentlab.chatbotbe.controller;

import com.scentlab.chatbotbe.constant.BespokeServiceUrls;
import com.scentlab.chatbotbe.dto.bespokeservice.request.CreateBookingRequest;
import com.scentlab.chatbotbe.entity.NoteDialogflowEntity;
import com.scentlab.chatbotbe.entity.json.CustomerProfile;
import com.scentlab.chatbotbe.repository.NoteDialogflowEntityRepository;
import com.scentlab.chatbotbe.service.ManageDialogflowService;
import com.scentlab.chatbotbe.utils.DialogflowService;
import com.scentlab.chatbotbe.utils.S3Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/test")
public class TestController {

    private final S3Service s3Service;
    private final DialogflowService dialogflowService;

    @Autowired
    public TestController(S3Service s3Service, DialogflowService dialogflowService) {
        this.s3Service = s3Service;
        this.dialogflowService = dialogflowService;
    }

    @GetMapping("/test-s3")
    public ResponseEntity<?> testGetS3(String src) throws IOException {
        s3Service.downloadS3Object(src);
        return ResponseEntity.ok("okay");
    }

    @PostMapping("/test-upload-s3")
    public ResponseEntity<?> testUploadS3(MultipartFile multipartFile) throws IOException {
        s3Service.uploadToS3(multipartFile);
        return ResponseEntity.ok("okay");
    }

//    @PostMapping("/test-create-entity")
//    public void a() throws IOException {
////        dialogflowService.createEntity("newagent-1-eyyiay");
//        Map<String, List<String>> entityValues = new HashMap<>();
//        entityValues.put("Banana", Arrays.asList("Banana", "Chuối"));
//        dialogflowService.updateEntity("newagent-1-eyyiay", "en-US", entityValues);
//    }

    @Autowired
    private NoteDialogflowEntityRepository noteDialogflowEntityRepository;

    @PostMapping("/insert-into-entity")
    public void b(
            @RequestBody NoteDialogflowEntity noteDialogflowEntity
            ) {
        noteDialogflowEntityRepository.save(noteDialogflowEntity);
    }

    @GetMapping("/get-all-entities")
    public ResponseEntity<List<Map>> getAllEntities(
            String projectId
    ) throws IOException {
        return ResponseEntity.ok(dialogflowService.listAllCurrentEntities(projectId));
    }

    @Autowired
    private ManageDialogflowService manageDialogflowService;

    @PostMapping("/update-note-vacancy")
    public void updateNoteVacancy(
            String entityId
    ) throws IOException {
        manageDialogflowService.updateNoteVacancyEntity(entityId);
    }

    @PostMapping("/update-perfume-vacancy")
    public void updatePerfumeVacancy(
            String entityId
    ) throws IOException {
        manageDialogflowService.updatePerfumeVacancyEntity(entityId);
    }

    @GetMapping("/test-web-client")
    public ResponseEntity<?> webClient() {
        CustomerProfile customerProfile = CustomerProfile.builder().gender("unisex").age(12).style("Label Operating System").typePerfume("Male").build();

        CreateBookingRequest createBookingRequest = CreateBookingRequest.builder()
                .userId("namns")
                .type("make for self")
                .makeAppointmentDate("2020-08-20")
                .timeBookingId("1")
                .customerMessage("Sent from Chatbot Backend")
                .perfumeId(Arrays.asList(1, 2, 3))
                .customerProfile(customerProfile)
                .build();

        Flux<String> stringFlux = WebClient.builder()
                .build()
                .post()
                .uri(BespokeServiceUrls.BOOKING_SERVICE_SAVE_URL)
                .accept(MediaType.ALL)
                .header("Content-Type", "application/json;charset=UTF-8")
                .body(BodyInserters.fromValue(createBookingRequest))
                .retrieve()

                .bodyToFlux(String.class);
        stringFlux.subscribe(e -> System.out.println("My response" + e));
        System.out.println("exit");
        return ResponseEntity.ok().body("f");
    }
}

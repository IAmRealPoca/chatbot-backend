package com.scentlab.chatbotbe.dto.bespokeservice.request;

import com.scentlab.chatbotbe.entity.json.CustomerProfile;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CreateBookingRequest {
    private String userId;
    private String makeAppointmentDate;
    private String timeBookingId;
    private String customerMessage;
    private String type;
    private CustomerProfile customerProfile;
    private List<Integer> perfumeId;
}

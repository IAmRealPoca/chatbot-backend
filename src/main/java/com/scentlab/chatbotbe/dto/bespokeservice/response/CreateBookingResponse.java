package com.scentlab.chatbotbe.dto.bespokeservice.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.scentlab.chatbotbe.entity.PerfumeFragrantica;
import com.scentlab.chatbotbe.entity.json.CustomerProfile;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Collection;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CreateBookingResponse {
    private String bookingId;
    private String username;
    private String fullname;
    private String type;
    private String status;
    private String dateBooking;
    private String timeStart;
    private String timeEnd;
    private String expertMessage;
    private String customerMessage;
    @JsonIgnore
    private Map<String, Object> attributes;
    private CustomerProfile customerProfile;
    private Collection<PerfumeFragrantica> perfumesPicked;
}

package com.scentlab.chatbotbe.dto.bespokeservice.response;

import com.scentlab.chatbotbe.entity.json.InfoCustomer;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GetUserBookingResponse {
    private String bookingId;
    private String username;
    private String fullname;
    private String type;
    private String status;
    private String dateBooking;
    private String timeStart;
    private String timeEnd;
    private String expertMessage;
    private String customerMessage;
    private InfoCustomer infoCustomer;

    @Override
    public String toString() {
        return "GetUserBookingResponse{" +
                "bookingId='" + bookingId + '\'' +
                ", username='" + username + '\'' +
                ", fullname='" + fullname + '\'' +
                ", type='" + type + '\'' +
                ", status='" + status + '\'' +
                ", dateBooking='" + dateBooking + '\'' +
                ", timeStart='" + timeStart + '\'' +
                ", timeEnd='" + timeEnd + '\'' +
                ", expertMessage='" + expertMessage + '\'' +
                ", customerMessage='" + customerMessage + '\'' +
                ", infoCustomer=" + infoCustomer +
                '}';
    }
}

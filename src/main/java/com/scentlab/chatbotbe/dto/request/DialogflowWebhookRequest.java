package com.scentlab.chatbotbe.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DialogflowWebhookRequest {
    private String responseId;
    private QueryResult queryResult;

    private Object originalDetectIntentRequest;

    private String session;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    private class QueryResult {
        private String queryText;
        private String action;
        private Map<String, Object> parameters;

        private Boolean allRequiredParamsPresent;

        private List<Object> fulfillmentMessages;
        private List<Map<String, Object>> outputContexts;

        private Map<String, Object> intent;
        private Double intentDetectionConfidence;

        private String languageCode;
    }
}

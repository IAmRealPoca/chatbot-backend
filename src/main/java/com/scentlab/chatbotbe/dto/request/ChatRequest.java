package com.scentlab.chatbotbe.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChatRequest {
    private String userName;
    private String sessionId;
//    private List<String> contexts;
//    private String action;
    private List<String> chatContent;
}

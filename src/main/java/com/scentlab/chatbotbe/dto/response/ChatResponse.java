package com.scentlab.chatbotbe.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ChatResponse {
    private Integer responseType;
    private Object response;
    private String time;
    private String sessionId;
    private List<String> contexts;
    private String action;
}

package com.scentlab.chatbotbe.dto.response;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DialogflowWebhookResponse {
    private Map<String, Object> fulfillmentMessages;

}

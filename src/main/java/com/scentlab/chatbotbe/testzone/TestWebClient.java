package com.scentlab.chatbotbe.testzone;

import com.scentlab.chatbotbe.constant.BespokeServiceUrls;
import com.scentlab.chatbotbe.dto.bespokeservice.request.CreateBookingRequest;
import com.scentlab.chatbotbe.entity.json.CustomerProfile;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;

import java.util.Arrays;

public class TestWebClient {
    public static void main(String[] args) throws InterruptedException {
        CustomerProfile customerProfile = CustomerProfile.builder().gender("unisex").age(12).style("Label Operating System").typePerfume("Male").build();

        CreateBookingRequest createBookingRequest = CreateBookingRequest.builder()
                .userId("namns")
                .type("make for self")
                .makeAppointmentDate("2020-08-20")
                .timeBookingId("1")
                .customerMessage("Sent from Chatbot Backend")
                .perfumeId(Arrays.asList(1, 2, 3))
                .customerProfile(customerProfile)
                .build();

        Flux<ClientResponse> stringFlux = WebClient.create()
                .post()
                .uri(BespokeServiceUrls.BOOKING_SERVICE_SAVE_URL)
                .body(BodyInserters.fromValue(createBookingRequest))
                .exchange()
                .flux();

//        while (true) {
            stringFlux.subscribe(e -> System.out.println("My response" + e.bodyToFlux(String.class)));
//        }
        Thread.sleep(50000);
        System.out.println("exit");


    }

}

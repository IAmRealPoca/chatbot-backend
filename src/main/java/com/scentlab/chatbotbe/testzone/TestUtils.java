package com.scentlab.chatbotbe.testzone;

import com.scentlab.chatbotbe.utils.UUIDUtils;
import org.apache.commons.text.similarity.LongestCommonSubsequence;

public class TestUtils {
    public static void main(String[] args) {
        LongestCommonSubsequence longestCommonSubsequence = new LongestCommonSubsequence();
//        String a = "Smith J Ph234567 34 Smith Rd";
        String a = "Simth Simth Simth Simth Simth";
        String b = "Smith J Ph234567 34 Smith Road";

        double length = Double.valueOf(longestCommonSubsequence.apply(a, b));
        System.out.println(length);
        double oneHundred = 100.0;

        double percent = length / Math.max(a.length(), b.length()) * oneHundred;

//        System.out.println(Math.min());
        System.out.println(percent);
        System.out.println();

        System.out.println(UUIDUtils.getUUID());
    }

    //                        for (Map.Entry<String, Value> entry : messages.getPayload().getFieldsMap().entrySet()) {
//                            switch (entry.getValue().getKindCase()) {
//                                case LIST_VALUE:
//
//                                    ListValue perfumeListValue = ListValue.newBuilder().build();
//                                    for (PerfumeFragrantica perfume : perfumeFragranticas) {
//                                        Value perfumeValue = Value.newBuilder().setStringValue(perfume.getName()).build();
//                                        perfumeListValue.toBuilder().addValues(perfumeValue).build();
//                                    }
//                                    Value perfumeValueList = Value.newBuilder().setListValue(perfumeListValue).build();
//                                    entry.setValue(perfumeValueList);
//
//                                    break;
//                            }
//                        }
}

package com.scentlab.chatbotbe.testzone;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.scentlab.chatbotbe.dto.bespokeservice.request.CreateBookingRequest;
import com.scentlab.chatbotbe.entity.json.CustomerProfile;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

public class TestRestTemplate {
    public static void main(String[] args) {
        try {
            CustomerProfile customerProfile = CustomerProfile.builder().gender("unisex").age(12).style("Label Operating System").typePerfume("Male").build();

            CreateBookingRequest createBookingRequest = CreateBookingRequest.builder()
                    .userId("namgg")
                    .type("make for self")
                    .makeAppointmentDate("2020-08-20")
                    .timeBookingId("1")
                    .customerMessage("Sent from Chatbot Backend")
                    .perfumeId(Arrays.asList(1, 2, 3))
                    .customerProfile(customerProfile)
                    .build();

            consumeRest(createBookingRequest);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    private static void consumeRest(CreateBookingRequest createBookingRequest) throws JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        String url = "http://localhost:8082/bookings/save";

        HttpEntity<CreateBookingRequest> requestObject = new HttpEntity<>(createBookingRequest);

        try {
            ResponseEntity<String> responseEntity = restTemplate.postForEntity(url, requestObject, String.class);
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode responseJson = objectMapper.readTree(responseEntity.getBody());
            System.out.println(responseJson);
        } catch (HttpClientErrorException ex) {
            if (ex.getStatusCode().is4xxClientError()) {
                System.out.println("404 error: " + ex.getResponseBodyAsString());
            }
        }


    }
}

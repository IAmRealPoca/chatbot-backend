package com.scentlab.chatbotbe.constant;

public class ApiPath {
    public static final String CHAT_PATH = "/chat";
    public static final String SEND_CHAT = "/send-chat";
}

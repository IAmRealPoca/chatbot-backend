package com.scentlab.chatbotbe.constant;

import lombok.Getter;

@Getter
public enum BookingStatusEnum {
    CREATED(1, "confirming"),
    CONFIRMED(2, "confirming"),
    DATE_BOOKED(3, "confirming"),
    DONE(4, "confirming"),
    ;

    private int number;
    private String statusString;

    BookingStatusEnum(Integer number, String status) {
        this.number = number;
        this.statusString = status;
    }


}

package com.scentlab.chatbotbe.constant;

public class ParameterConstants {
    public static final String DIALOGFLOW_NOTE_VACANCY = "note-vacancy";
    public static final String DIALOGFLOW_PERFUME_VACANCY = "perfume-vacancy";

//    public static final String DIALOGFLOW_CONTEXT_

    public static final String CUSTOMPAYLOAD = "customPayload";
    public static final String CUSTOMPAYLOAD_CUSTOMDATA = "customData";
    public static final String CUSTOMPAYLOAD_MESSAGE = "message";
    public static final String CUSTOMPAYLOAD_WEBHOOK_ERROR_TYPE = "perfumePickErrorType";


}

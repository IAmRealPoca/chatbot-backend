package com.scentlab.chatbotbe.constant;


public enum BespokeDialogFlowMessageCase {
    TEXT(1),
    PAYLOAD(5),

    CUSTOMER_INFO_OBJECT(10),
    QUERYRESULT_CONTEXTS(11),
    QUERYRESULT_ACTION(12),
    CUSTOMIZED_PAYLOAD(13),
    ERROR(-1)
    ;
    private int value;

    BespokeDialogFlowMessageCase(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public int getNumber() {
        return getValue();
    }
}

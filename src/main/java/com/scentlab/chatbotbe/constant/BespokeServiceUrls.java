package com.scentlab.chatbotbe.constant;

public class BespokeServiceUrls {

    //    @Value("${bespoke.service.booking-service}")
//    public static String BOOKING_SERVICE_SAVE_URL = "http://localhost:8082/bookings/save";
    public static final String BOOKING_SERVICE_BASE_URL = "https://scenlab-booking.herokuapp.com";
    public static final String BOOKING_SERVICE_SAVE_URL = BOOKING_SERVICE_BASE_URL + "/bookings/save";
    public static final String BOOKING_SERVICE_FIND_BY_USERNAME_URL = BOOKING_SERVICE_BASE_URL + "/bookings/customers";
}

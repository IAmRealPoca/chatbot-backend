package com.scentlab.chatbotbe.objmapper;

import com.scentlab.chatbotbe.entity.json.SessionContent;
import org.mapstruct.Mapper;
import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Mapper(componentModel = "spring")
public abstract class SessionContentMapper {
    public SessionContent fromMapToSessionContent(Map<String, Object> sessionContentMap) {
        if (CollectionUtils.isEmpty(sessionContentMap)) {
            return null;
        }
        SessionContent sessionContent = new SessionContent();
        Object sessionContentObj = sessionContentMap.get("sessionContent");
        if (sessionContentObj instanceof SessionContent) {
            sessionContent = (SessionContent) sessionContentObj;
            return sessionContent;
        } else {
            Map<String, Object> innerMap = (Map<String, Object>) sessionContentObj;
            sessionContent.setAction((String) innerMap.get("action"));
            sessionContent.setContexts((List<String>) innerMap.get("contexts"));
            sessionContent.setParameters((Map<String, Object>) innerMap.get("parameters"));

            List<String> selectedPerfumes = (List<String>) innerMap.get("selectedPerfumes");
            Set<String> selectedPerfumeSet = null;
            if (!CollectionUtils.isEmpty(selectedPerfumes)) {
                for (String perfume : selectedPerfumes) {
                    if (selectedPerfumeSet == null) {
                        selectedPerfumeSet = new HashSet<>();
                    }
                    selectedPerfumeSet.add(perfume);
                }
            }
            sessionContent.setSelectedPerfumes(selectedPerfumeSet);
            return sessionContent;
        }


    }
}
